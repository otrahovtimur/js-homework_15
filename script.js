
// ТЕОРІЯ
/* 1. setInterval - подія/дія буде відбуватися багато разів з вказаною періодичністю
       setTimeout - подія/дія відбудеться 1 раз через вказаний час 

 2. Так, можна

 3. clearTimeout , clearInterval */

 // ПРАКТИКА

 //1
 const changeBtn = document.getElementById('changeBtn');

function changeContent() {
    const contentDiv = document.getElementById('contentDiv')
    contentDiv.textContent = "Зміни внесено"
    console.log( "Зміни внесено");
};

changeBtn.addEventListener('click',() => {
    setTimeout(changeContent, 3000)
} );

//2 
function startCountdown () {
    let startTime = 10;

    const timerElement = document.getElementById('timer');
    timerElement.textContent = startTime;

    const intervalId = setInterval(() => {
        startTime--;
        timerElement.textContent = startTime;

        if(startTime <= 0) {
            clearInterval(intervalId);
            setTimeout(() => {
                console.log('Ваш час вийшов');
                timerElement.textContent = 'Зворотній відлік завершено';
        }, 0);
    }
    }, 1000);
};
const startTimerBtn = document.getElementById("startTimer");
startTimerBtn.addEventListener('click', startCountdown);



